#include <Arduino.h>
/*
 ******************************************************
 *              LIBRERIAS y CONSTANTES
 ******************************************************
*/
const int ejeX_pin = A0; 
const int ejeY_pin = A1;
const int SW_pin = 13; // click

int xValue, yValue, botonValue =  0;

/*
 ******************************************************
 *              SETUP CONFIGURACIÓN INICIAL
 ******************************************************
 */
void setup() {
  pinMode(SW_pin, INPUT);
  digitalWrite(SW_pin, HIGH);
  Serial.begin(9600);
}

/*
 ******************************************************
 *              LOOP BUCLE PRINCIPAL
 ******************************************************
 */
void loop() {
  xValue = analogRead(ejeX_pin);
  yValue = analogRead(ejeY_pin);
  botonValue = digitalRead(SW_pin);

  Serial.print(xValue,DEC);
	Serial.print(",");
	Serial.print(yValue,DEC);
	Serial.print(",");
	Serial.print(!botonValue);
 	Serial.print("\n");
  delay(10);
}
